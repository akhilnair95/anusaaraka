#include <stdio.h>
#include <string.h>
#include <gdbm.h>
#include <stdlib.h>
#include <stddef.h>
#include "/usr/include/gdbm.h"
#define datum_set(um, buf) { um.dptr = buf; um.dsize = strlen(buf); }
void main(){
	GDBM_FILE dbf;
	datum key,content;

	char *name = "/home/sampada/anusaaraka/Akhil/default-iit-bombay-shabdanjali-dic.gdbm";
	dbf =  gdbm_open (name, 512, GDBM_WRITER, 0644, 0);
 
	datum_set(key, "1");
	datum_set(content, "akhil");

	gdbm_store(dbf,key,content,GDBM_REPLACE);
	
	key = gdbm_firstkey (dbf);

	while (key.dptr != NULL){
		datum nextkey;
		// do something with the key 
		content = gdbm_fetch(dbf,key);
		// Obtain the next key 
		nextkey = gdbm_nextkey(dbf, key);

		printf("%s %s \n",content.dptr,key.dptr);
		// Reclaim the memory used by the key 
		free (key.dptr);
		// Use nextkey in the next iteration. 
		key = nextkey;
	}
	//content = gdbm_fetch(dbf,key);
	//printf("%s %s \n",content.dptr,key.dptr);
	gdbm_close(dbf);
}
