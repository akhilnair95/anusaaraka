#include <stdio.h>
#include <string.h>
#include <gdbm.h>
#include <stdlib.h>
#include <stddef.h>
#include "/usr/include/gdbm.h"
#define datum_set(um, buf) { um.dptr = buf; um.dsize = strlen(buf); }

char *replace_str(char *str, char *orig, char *rep){
    char buffer[4096];
    char *p;
     
    if(!(p = strstr(str, orig)))
    	return str;
     
    strncpy(buffer, str, p-str);
    buffer[p-str] = '\0';
     
    sprintf(buffer+(p-str), "%s%s", rep, p+strlen(orig));
     
    return strdup(buffer);
}


int main(int argc, char *argv[]){
	GDBM_FILE dbf;
	datum key,old_mng,new_mng;
	
	char *eng = argv[1];
	char *new = argv[2];	

	char *name = "../Anu_databases/default-iit-bombay-shabdanjali-dic.gdbm";

	dbf =  gdbm_open(name, 512, GDBM_WRITER, 0644, 0);
 
	datum_set(key,eng);
	
	datum_set(new_mng,new);
	gdbm_store (dbf,key,new_mng,GDBM_REPLACE);
	
	gdbm_close(dbf);
	
	return 0;
}


