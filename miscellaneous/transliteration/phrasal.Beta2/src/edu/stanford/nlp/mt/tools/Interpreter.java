package edu.stanford.nlp.mt.tools;

/**
 * @author Michel Galley
 */

public interface Interpreter {
  public Object evalString(String s);
}
