if ! [ -d $4/tmp/$1_tmp/$2 ] ; then
    echo "Sentence $2 does not exist in $1"
    exit
 fi

 MYPATH=$4/tmp

 cd $MYPATH/$1_tmp/$2
 echo "(defglobal ?*path* = $HOME_anu_test)" > global_path.clp
 echo "(defglobal ?*provisional_wsd_path* = $HOME_anu_provisional_wsd_rules)" >> global_path.clp
 echo "(Parser_used Stanford-Parser)" >> parser_type.dat
 echo "(Domain $5)" >> domain.dat
 
 #STAGE 2.1
 cd $HOME_anu_test/Anu_src/
 ./constituency_parse $MYPATH/$1_tmp/$2/E_constituents_info_tmp.dat  $MYPATH/$1_tmp/$2/Node_category_tmp.dat < $MYPATH/$1_tmp/$2/sd-lexicalize_info.dat

 cd $MYPATH/$1_tmp/$2
 sed "/\[[0-9]*[:][0-9]*\][ ][(]active[)]/d" $MYPATH/$1_tmp/$2/logon_output.txt |sed -n -e "H;\${g;s/)\n\n{/)\n\n;~~~~~~~~~~\n{/g;p}" | sed -n -e "H;\${g;s/[[][0-9]*[]][ ][(][0-9]*[ ]of[ ][0-9]*[)][ ][{][0-9]*[}][ ][\`]\(.*\)[']\(.*\)\n[;]\~\~\~\~\~\~\~\~\~\~\n/\`\1\'\2\n;\~\~\~\~\~\~\~\~\~\~\n\`\1\'\n/g;p}" | sed -n -e "H;\${g;s/}\n\n{/}\n\n;~~~~~~~~~~\n{/g;p}" | sed -n -e "H;\${g;s/\n\n[\n]*/\n/g;p}" > $MYPATH/$1_tmp/$2/logon_output_tmp.txt
 
 #STAGE 2.2
 csplit -f logon_output -b '%d.txt' $MYPATH/$1_tmp/$2/logon_output_tmp.txt '/;\~\~\~\~\~\~\~\~\~\~/' '{*}' > /dev/null

 mv $MYPATH/$1_tmp/$2/logon_output0.txt $MYPATH/$1_tmp/$2/logon_derivation_parse.txt
 mv $MYPATH/$1_tmp/$2/logon_output1.txt $MYPATH/$1_tmp/$2/logon_dependency_parse.txt
 mv $MYPATH/$1_tmp/$2/logon_output2.txt $MYPATH/$1_tmp/$2/logon_triples_parse.txt

 sed -i -n -e "H;\${g;s/[;]\~\~\~\~\~\~\~\~\~\~\n//g;p}" $MYPATH/$1_tmp/$2/logon_dependency_parse.txt
 sed -i -n -e "H;\${g;s/[;]\~\~\~\~\~\~\~\~\~\~\n//g;p}" $MYPATH/$1_tmp/$2/logon_triples_parse.txt

 #STAGE 2.3
 sed "s/{//g" $MYPATH/$1_tmp/$2/logon_triples_parse.txt | sed "s/}//g" | sed "/^$/d" | sed "s/^/(logon_relation-properties /g" | sed "s/$/)/g" >$MYPATH/$1_tmp/$2/logon_triples.dat

 $HOME_anu_test/Parsers/logon-parser/src/derivation_parse < $MYPATH/$1_tmp/$2/logon_derivation_parse.txt $MYPATH/$1_tmp/$2/logon_derivation_tree.dat $MYPATH/$1_tmp/$2/logon_category.dat

 $HOME_anu_test/Parsers/logon-parser/src/dependency_parse < $MYPATH/$1_tmp/$2/logon_dependency_parse.txt $MYPATH/$1_tmp/$2/logon_relations.dat 
 
