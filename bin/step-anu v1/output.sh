
 MYPATH=$HOME_anu_tmp

 cd $HOME_anu_test/bin/step-anu/
 sh run_sentence_output.sh sample 2.1 1 $MYPATH $4

 echo "Calling Transliteration"
 cd $HOME_anu_test/miscellaneous/transliteration/work
 sh run_transliteration.sh $MYPATH/tmp sample

 cd $MYPATH/tmp/sample_tmp/
 echo "(defglobal ?*path* = $HOME_anu_test)" > path_for_html.clp
 echo "(defglobal ?*mypath* = $MYPATH)" >> path_for_html.clp
 echo "(defglobal ?*filename* = ""sample"")" >> path_for_html.clp

 echo "Calling Interface related programs"
 sh $HOME_anu_test/bin/run_anu_browser.sh $HOME_anu_test sample $MYPATH $HOME_anu_output
