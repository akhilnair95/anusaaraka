export HOME_anu=/home/akhil/Documents/anusaaraka/
export LOGONTMP=$HOME_anu/LOGON/logon_tmp 
export LOGONROOT=$HOME_anu/LOGON/logon
export HOME_anu_test=$HOME_anu
export HOME_anu_output=$HOME_anu/Anu_outputs
export HOME_anu_tmp=$HOME_anu/Temp
export HOME_anu_provisional_wsd_rules=$HOME_anu/WSD/wsd_rules
export PATH=/usr/lib/jvm/java-8-openjdk-amd64/bin:$HOME_anu_test/bin:$PATH
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export LD_LIBRARY_PATH=/usr/local/lib/ 
export http_proxy=http://netmon.iitb.ac.in:8080

export LC_ALL=
export LC_ALL=en_US.UTF-8

exec 2>/home/akhil/temp
cd /home/akhil/Documents/anusaaraka/bin/step-anu/
sh run_parsers.sh
