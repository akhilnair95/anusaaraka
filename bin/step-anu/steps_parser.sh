myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/parser_tree_correction_rules.bat >  $HOME_anu_test/logs/1.log
myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/parser_tree_modification_rules.bat >  $HOME_anu_test/logs/2.log
myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/sd_combining_apostrophe.bat >  $HOME_anu_test/logs/3.log
myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/replace_head_word_with_id.bat >  $HOME_anu_test/logs/4.log
myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/handling_punctuations.bat >  $HOME_anu_test/logs/5.log
myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/parserid_wordid_mapping.bat >  $HOME_anu_test/logs/6.log
myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/sd_category.bat >  $HOME_anu_test/logs/7.log
myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/cat_rule_step.bat >  $HOME_anu_test/logs/8.log

cat parser_cat.dat parser_cat_coarse.dat > parser_pos_cat.dat

sort -V -k 2 parser_cat.dat > tmp
rm parser_cat.dat
mv tmp parser_cat.dat


sort -V -k 2 parser_cat_coarse.dat > tmp
rm parser_cat_coarse.dat
mv tmp parser_cat_coarse.dat

myclips -f $HOME_anu_test/Anu_clp_files/modules_trace/root_rule.bat >  $HOME_anu_test/logs/9.log

sort -V -k 2 preferred_morph_tmp.dat > tmp
rm preferred_morph_tmp.dat
mv tmp preferred_morph_tmp.dat
