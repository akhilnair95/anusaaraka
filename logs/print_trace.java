import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class print_trace {
	
	static ArrayList<String> added_facts = new ArrayList<String>();
	static ArrayList<String> rules_fired = new ArrayList<String>();
			
	public static void main(String[] args) {
		String map[]; 
		
		int dat = Integer.parseInt(getfile("logfile"));

		map = get_line_map(dat+1+".log");
				
		int line = find_assert_line(map,Clean(getfile("name")));		
		
		// check for printed <fact>
		if(line == -1){
			line = find_assert_line(map,getfile("name"));
		}
				
			
		if(line != -1){
			print_map(map,map[line].split(" ")[1],line);
			write_rules_fired();
		}
		else
			System.out.println("error");
				
	}


	private static void write_rules_fired() {
		PrintWriter writer;
		
		try {
			writer = new PrintWriter("rules_fired", "UTF-8");
			
			for(int i=0;i<rules_fired.size();i++)
				writer.println(rules_fired.get(i));
			
			writer.close();
			
		} catch (Exception e) {	e.printStackTrace();}
	}


	private static String Clean(String str) {
		return str.replaceAll("	", " ").replaceAll("( )+", " ").trim();
	}


	private static String getfile(String name) {
		try {
			Scanner scn = new Scanner(new File(name));
			String line = scn.nextLine();
			scn.close();
			return line;
		} catch (FileNotFoundException e) {e.printStackTrace();}
		return null;
	}

	private static void print_map(String[] map, String fact , int as_line) {
			/*
			 * Module to find a rule / database from which 
			 * a fact<f-NO> is derived, and recursively print 
			 * its traceback graph
			 */
		
			added_facts.add(fact);
			
		
						
			for(int i = as_line; i > 0 ; i--){
				
				 if(map[i].contains("FIRE")){
					 // If formed from rules
					 
					 String rule = get_rule_from_line(map[i]);
					 
					 if(!rules_fired.contains(rule))
						 rules_fired.add(rule);
					 
					 
					 String cond[] = Clean(map[i]).split(" ")[3].split(",");
					 
					 for(int j=0;j<cond.length;j++){
						 // If cond = "*" just avoid or if already drawn fact, just avoid
						 
						 if(!cond[j].matches("\\*"))
							 {
							 if(!added_facts.contains(cond[j]))
							 	print_map(map,cond[j],find_assert_line(map,cond[j]+" "));
						 }
						 
					 }
					 
					 return;
				 }
				 
				 else if(map[i].contains("(load-facts")){
					 // If formed from load facts
					 String file = map[i].substring(map[i].indexOf('"'), map[i].lastIndexOf('"'));
					 // set label for file
					 set_file_label(fact,file,map);
					 
					 // put connection from c[var+1] -> c[var] {file -> fact}
					 //set_connection("d"+fact, fact);
					 
					 return;
				 }
				 
			 }
			
			System.out.println("err[label = \"error\"]");
	}

	
	private static String get_rule_from_line(String str) {
		return Clean(str).split(" ")[2].replace(":", "");	
	}


	private static void set_file_label(String fact, String label, String[] map) {
		label = label.replace('"', ' ').trim();
		System.out.println(get_label(map,find_assert_line(map,fact+" ")));
		System.out.println(label);
		System.out.println();
	}
	
	
	private static int find_assert_line(String[] map, String fact) {
		// returns the line# in which the fact was <last> asserted
		for(int i=map.length - 1; i >= 0;i--){
			if(map[i].contains(fact) && map[i].contains("==>") && !map[i].contains("Activation")){
				return i;
			}
		}
		
		return -1;
	}
	
	
	private static String get_label(String[] map, int line_no) {
		//System.out.println(find_assert_line(map,fact));
		
		String line = map[line_no]; 		
		int index = line.indexOf("(");
		return line.substring(index,line.length());
	}

	private static String[] get_line_map(String file) {

		try {
			Scanner scn = new Scanner(new File(file));
			int i = 0;
			
			while(scn.hasNextLine()){
				scn.nextLine();
				i++;
			}
			
			String map[] = new String[i];
			
			scn.close();
			
			scn = new Scanner(new File(file));
			
			i = 0;
			
			while(scn.hasNextLine()){
					map[i] = Clean(scn.nextLine());
					i++;
				}
						
			scn.close();
			
			return map;
			
		} catch (FileNotFoundException e) {e.printStackTrace();}
		
		
		return null;

	}
	
}
