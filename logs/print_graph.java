import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class print_graph {
	/*
	 *  Written By : Akhil s.
	 *	Input Clips log file and a fact 
	 *  Output graph in dot recognizable format
	 */
	
	static ArrayList<String> added_facts = new ArrayList<String>();
	
	public static void main(String[] args) {
		String map[]; 
		
		map = get_line_map(getfile("logfile_ft"));

		int line = find_assert_line(map,Clean(getfile("name")));		
		
		// check for printed <fact>
		if(line == -1){
			line = find_assert_line(map,getfile("name"));
		}
		
		
		// Set up preliminary stuff
		System.out.println("digraph Test { \n");
		
		if(line != -1)
			print_map(map,map[line].split(" ")[1],line);
		else
			System.out.println("error");
		
		System.out.println("}");
		
	}


	private static String Clean(String str) {
		return str.replaceAll("	", " ").replaceAll("( )+", " ").trim();
	}


	private static String getfile(String name) {
		try {
			Scanner scn = new Scanner(new File(name));
			String line = scn.nextLine();
			scn.close();
			return line;
		} catch (FileNotFoundException e) {e.printStackTrace();}
		return null;
	}

	private static void print_map(String[] map, String fact , int as_line) {
			/*
			 * Module to find a rule / database from which 
			 * a fact<f-NO> is derived, and recursively print 
			 * its traceback graph
			 */
		
			// Add the facts to the linked list
			added_facts.add(fact);
			
			
			// declaring c$var [label = "LABEL"]
			set_var_label(fact,get_label(map,as_line));
						
			for(int i = as_line; i > 0 ; i--){
				
				 if(map[i].contains("FIRE")){
					 // If formed from rules
					 
					 //String rule = map[i].replace("    ", " ").split(" ")[2];
					 //rule = rule.replace(":", "");
					 
					 String rule = get_rule_from_line(map[i]);
					 
					 set_rule_label(fact,rule);
					 
					 // set connection from rule to fact
					 set_connection(get_rule(fact),fact);
					 
					 String cond[] = Clean(map[i]).split(" ")[3].split(",");
					 
					 for(int j=0;j<cond.length;j++){
						 // If cond = "*" just avoid or if already drawn fact, just avoid
						 
						 if(!cond[j].matches("\\*"))
							 // Set connections from cond to rule
							 {
							 set_connection(cond[j],get_rule(fact));
							// cond[j] + " " so that f-196 does not match f-19
							 if(!added_facts.contains(cond[j]))
							 	print_map(map,cond[j],find_assert_line(map,cond[j]+" "));
						 }
						 
					 }
					 
					 return;
				 }
				 
				 else if(map[i].contains("(load-facts")){
					 // If formed from load facts
					 String file = map[i].substring(map[i].indexOf('"'), map[i].lastIndexOf('"'));
					 // set label for file
					 set_file_label(fact,file);
					 
					 // put connection from c[var+1] -> c[var] {file -> fact}
					 set_connection("d"+fact, fact);
					 
					 return;
				 }
				 
			 }
			
			System.out.println("err[label = \"error\"]");
	}

	
	private static String get_rule_from_line(String str) {
		return Clean(str).split(" ")[2].replace(":", "");	
	}


	private static void set_file_label(String fact, String label) {
		label = label.replace('"', ' ');
		fact = fact.replace("-", "");
		System.out.println("	d" + fact +" [ label = \" " + label  + "\" shape = box3d];" );
	}

	private static void set_rule_label(String fact, String label){
		label = label.replace('"', ' ');
		fact = fact.replace("-", "");
		System.out.println("	" + get_rule(fact) +" [ label = \" " + label + "\" shape = box];" );
	}
	
	private static void set_var_label(String fact, String label){
		
		label = label.replace('"', ' ');
		fact = fact.replace("-", "");
		
		System.out.println("	" + fact +" [ label = \" " + label + "\"];" );
	}
	
	private static void set_connection(String par, String child){
		par = par.replace("-", "");
		child = child.replace("-", "");
		System.out.println("	"+par + " -> "+ child  );
	}
	
	private static int find_assert_line(String[] map, String fact) {
		// returns the line# in which the fact was asserted
		for(int i=map.length - 1; i >=0 ;i--){
			if(map[i].contains(fact) && map[i].contains("==>") && !map[i].contains("Activation")){
				return i;
			}
		}
		
		return -1;
	}
	

	
	private static String get_rule(String fact){
		return fact.replace('f', 'r').replace("p", "rp");
	}
	
	private static String get_label(String[] map, int line_no) {
		//System.out.println(find_assert_line(map,fact));
		
		String line = map[line_no]; 		
		int index = line.indexOf("(");
		return line.substring(index,line.length());
	}

	private static String[] get_line_map(String file) {

		try {
			Scanner scn = new Scanner(new File(file));
			int i = 0;
			
			while(scn.hasNextLine()){
				scn.nextLine();
				i++;
			}
			
			String map[] = new String[i];
			
			scn.close();
			
			scn = new Scanner(new File(file));
			
			i = 0;
			
			while(scn.hasNextLine()){
					map[i] = Clean(scn.nextLine());
					i++;
				}
						
			scn.close();
			
			return map;
			
		} catch (FileNotFoundException e) {e.printStackTrace();}
		
		
		return null;

	}
	
}
