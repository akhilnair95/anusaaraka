import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


public class get_ip_log{

	static int log_num;
	static String doc;
	
	public static void main(String[] args) {
		
		
		/*
		 * Prints step num in which the file in doc last came as output 
		 * 					to logfile
		 */
		
		try {
			Scanner scn = new Scanner(new File("logfile"));
			log_num = scn.nextInt();
			scn.close();
			
			scn = new Scanner(new File("doc"));
			doc = scn.nextLine();
			scn.close();
			
			while(log_num >=0 ){
				if(check(log_num,doc)){
					write(log_num);
					break;
				}
				log_num --;
			}
			
			if(log_num < 0)
				System.out.println("Error");
		
		} catch (FileNotFoundException e) {	e.printStackTrace();}
		
	}
	
	private static void write(int log_num) {
		PrintWriter writer;
		Scanner scn;
		String filename = "";
		
		try {
			writer = new PrintWriter("logfile", "UTF-8");
			writer.print(log_num - 1);
			writer.close();
		
			scn = new Scanner(new File("names"));
			int  i = 0;
			
			while(i != log_num){
				filename = scn.nextLine();
				i ++;
			}

			System.out.println(filename);

			scn.close();
		} catch (Exception e) {	e.printStackTrace();}
		
}

	private static boolean check(int log_num, String doc) throws FileNotFoundException {
		Scanner scn = new Scanner(new File("dat/out/"+log_num +".out"));
		
		while(scn.hasNextLine()){
			if(scn.nextLine().contains(doc)){
				scn.close();
				return true;
			}
		}
		scn.close();
		
		return false;
	}

}

