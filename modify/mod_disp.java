import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;


public class mod_disp {

	/*
	 * Converts the rule in rule.clp 
	 * 			from clp to csv format 
	 * O/p 	
	 * 		head.dat -> the titles and headings
	 * 		Cond.dat -> the Conditions
	 * 		actions.dat -> the Action Part
	 * 	 
	 */
	
	static ArrayList<String> heads;
	static ArrayList<String> conditions;
	static ArrayList<String> actions;
	
	public static void main(String[] args) {
		ArrayList<String> file = null;
		
		Scanner scn;
			try {
				scn = new Scanner(new File("rule.clp"));
				
				file = get_line_list(scn);
				
				write_headers(file);
				
				conditions = new ArrayList<String>();
				actions = new ArrayList<String>();

				
				for(int i=0;i<heads.size();i++){
					conditions.add("");
					actions.add("");
				}
				
				put_conditions(file);
				
				put_actions(file);
				
				for(int i=0;i<heads.size();i++){
					System.out.println(heads.get(i));
					System.out.println(conditions.get(i));
					System.out.println(actions.get(i));
				}
				
				scn.close();
			} catch (Exception e) {e.printStackTrace();}
	}

	private static void write_headers(ArrayList<String> file) throws FileNotFoundException, UnsupportedEncodingException {
		
		String line;
		
		heads = new ArrayList<String>();
		
		PrintWriter writer = new PrintWriter("head.dat", "UTF-8");
				
		// Trailing away unwanted lines
		int i = 0;
		
		while(!file.get(i).contains("defrule"))
			i++;
		
		line = file.get(i);
		
		writer.println(Clean(line).split(" ")[1]);

		// Trailing away unwanted lines
		while(!file.get(i).contains("salience"))
			i++;
		
		line = file.get(i);
		
		line = line.substring(line.indexOf("salience") + 8).trim();
		line = line.substring(0,line.indexOf(')'));
		
		writer.println(line);
		
		for(i = i + 1;i<file.size();i++){
			line = Clean(file.get(i));
			
			if(line.isEmpty() || line.startsWith(";") || line.startsWith("=>") || line.startsWith(")") || line.contains("retract"))
				continue;
			
			if(line.contains("assert")){
				line = line.substring(line.indexOf("assert") + 6,line.length() - 1).trim();
			}
			
			if(!line.contains("assert")){
				// ( HEAD LIST )
				if(line.contains("<-"))
					line = line.substring(line.indexOf("<-") + 2);
				
				line = line.substring(1).trim().split(" ")[0];
				
				if(!heads.contains(line)){
					heads.add(line);
				}
			}
			
		}
		
		writer.close();
	}
	
	private static void put_conditions(ArrayList<String> file)  {

		String line;
		boolean retract;		
		String head,body;
		int index;
		
		// Trailing away unwanted lines
		int i = 0;
	
		while(!file.get(i).contains("salience"))
			i++;
		
		
		for(i = i + 1;i<file.size();i++){
			line = Clean(file.get(i));
			
			if(line.contains("=>"))
				break;
			
			if(line.isEmpty() || line.startsWith(";"))
				continue;
			
			retract = false;
			
			if(line.contains("<-")){
				line = line.substring(line.indexOf("<-") + 2);
				retract = true;
			}
			
			line = line.substring(1).trim();
			head = line.split(" ")[0];
			body = line.substring(head.length()+1, line.length()-1);
			
			index = heads.indexOf(head);
			
			conditions.set(index, conditions.get(index).concat(body+","+retract+","));
			
		}
		
	}

	private static void put_actions(ArrayList<String> file)  {
		String line;
		String head,body;
		int index;
		boolean funct;
		
		// Trailing away unwanted lines
		int i = 0;
	
		while(!file.get(i).contains("=>"))
			i++;
		
		
		for(i = i + 1;i<file.size();i++){
			line = Clean(file.get(i));
			
			if(line.isEmpty() || line.startsWith(";") || line.contains("retract") || line.startsWith(")"))
				continue;
			
			funct = true;
			
			if(line.contains("assert")){
				line = line.substring(line.indexOf("assert") + 6,line.length() - 1).trim();
				funct = false;
			}
						
			line = line.substring(1).trim();
			head = line.split(" ")[0];
			
			if(line.matches(head))
				continue;
				
			body = line.substring(head.length()+1, line.length()-1);
			
			index = heads.indexOf(head);
			
			actions.set(index, actions.get(index).concat(body+","+funct+","));
			
		}
	}

	private static ArrayList<String> get_line_list(Scanner scn) {
		ArrayList<String> lines = new ArrayList<String>();
		while(scn.hasNextLine())
			lines.add(scn.nextLine());
		return lines;
	}
	
	private static String Clean(String str) {
		return str.replace("	", " ").replaceAll("( )+", " ").trim();
	}

}

