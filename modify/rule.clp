 (defrule same_cat
 (declare (salience 150))
 (parser_id-cat_coarse ?pid ?cat)
 (parserid-wordid  ?pid ?wid)
 (id-original_word ?wid ?word)
 (word-morph (original_word ?word)(morph_word ?morph_wrd)(root ?root)(category ?cat)(suffix ?suf)(number ?num))
 ?f0<-(morph_analysis_to_be_choosen ?wid)
 =>
        (retract ?f0)
	(printout t "==> p-1 "  "(parser_id-root-category-suffix-number  "?pid"  "?root" "?cat " "?suf" " ?num")" crlf)
        (printout ?*pre_morph_fp* "(parser_id-root-category-suffix-number  "?pid"  "?root" "?cat " "?suf" " ?num")" crlf)
	(printout t "==> p-2 "   "(parser_id-root "?pid" "?root")" crlf)
        (printout ?*root_fp*  "(parser_id-root "?pid" "?root")" crlf)
 )
 ;-----------------------------------------------------------------------------------------------------------------------
 ;I biked Johnson Creek .
 ;Bushes are visiting us today   (number info yet to be taken)
