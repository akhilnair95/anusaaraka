import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class get_heads2 {
	
	static ArrayList<String> heads;
	
	public static void main(String[] args) {
		heads = new ArrayList<String>();
		addExistingHeads();
		addNewHeads();
		printHeads();
	}

	private static void printHeads() {
		PrintWriter writer;
		
		try {
			writer = new PrintWriter("head.dat", "UTF-8");
			for(int i=0;i<heads.size();i++){
				writer.println(heads.get(i));
			}
			writer.close();
		} catch (Exception e) {	e.printStackTrace();}
	}

	private static void addNewHeads() {
			try {
				Scanner scn = new Scanner(new File("files"));
				while(scn.hasNextLine()){
					String line = scn.nextLine();
					addHeadsFromFile(line);
				}
				scn.close();
			} catch (FileNotFoundException e) {e.printStackTrace();}
	}

	private static void addHeadsFromFile(String file) throws FileNotFoundException {
		Scanner scn = new Scanner(new File("../Temp/tmp/sample_tmp/2.1/"+file));
			while(scn.hasNextLine()){
				String line = scn.nextLine().trim();
				if(line.isEmpty() || line.startsWith(";"))
					continue;
				String head = line.split(" ")[0].replace("(", "");
				if(!heads.contains(head))
					heads.add(head);
			}
		scn.close();
	}

	private static void addExistingHeads() {
		try {
			Scanner scn = new Scanner(new File("head.dat"));
			while(scn.hasNextLine())
				heads.add(scn.nextLine());
			scn.close();
		} catch (FileNotFoundException e) {	e.printStackTrace();}
	}

}

