import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


public class extract {

	/*
	 * Opens orig.clp
	 * Extracts needed rule to rule.clp 
	 * rest into remain.clp
	 */
	public static void main(String[] args) {
		String line = null;
		
		try{
			
			Scanner scn = new Scanner(new File("orig.clp"));
			String rule_name = read_line("rule_name");
						
			PrintWriter writer_rem = new PrintWriter("remain.clp", "UTF-8");
			PrintWriter writer_rule = new PrintWriter("rule.clp", "UTF-8");
			
			while(scn.hasNextLine()){
				line=scn.nextLine();
				if(line.contains("defrule") && line.contains(rule_name))
					break;
				writer_rem.println(line);
			}
			
			//if not End by EOF , write line to rule.clp
			if(scn.hasNextLine())
				writer_rule.println(line);
			
			while(scn.hasNextLine()){
				line=scn.nextLine();
				if(line.contains("defrule"))
					break;
				writer_rule.println(line);
			}
			
			//if not End by EOF , write line to rem.clp
			if(scn.hasNextLine())
				writer_rem.println(line);
			
			while(scn.hasNextLine())
				writer_rem.println(scn.nextLine());
			
			
			writer_rem.close();
			writer_rule.close();
			
			scn.close();
		}catch(Exception e){e.printStackTrace();}
	}

	static String read_line(String file) throws FileNotFoundException{
		Scanner scn = new Scanner(new File("rule_name"));
		String line;
		
		if(scn.hasNextLine())
			line = scn.nextLine();
		
		else
			line = "ERROR";
		
		scn.close();
		
		return line;
	}
	
}

