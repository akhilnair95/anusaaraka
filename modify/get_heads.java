import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class get_heads {

	public static void main(String[] args) {
		try {
			Scanner scn = new Scanner(new File("batchfile"));
			ArrayList<String> heads = new ArrayList<String>(); 
					
			while(scn.hasNextLine()){
				String line = scn.nextLine();
				if(line.contains("load-facts"))
					System.out.println(line.substring(line.indexOf('"') + 1, line.lastIndexOf('"')));
				else if(line.contains("open")){
					line = line.substring(line.indexOf('"')+ 1);
					line = line.substring(0,line.indexOf('"'));
					System.out.println(line);
				}
				else if(line.contains("save-facts")){
					line = Clean(line);
					line = line.split(" ")[3].replace(")", "");
					heads.add(line);
				}
			}
			scn.close();
			
			Write(heads);
		} catch (FileNotFoundException e) {	e.printStackTrace();}
	}
	
	private static void Write(ArrayList<String> line) {
		PrintWriter writer;
		
		try {
			writer = new PrintWriter("head.dat", "UTF-8");
			for(int i=0;i<line.size();i++){
				writer.println(line.get(i));
			}
			writer.close();
		} catch (Exception e) {	e.printStackTrace();}
	}

	private static String Clean(String str) {
		return str.replaceAll("( )+", " ").trim();
	}

}

