(defrule cp_fact
(declare (salience 2001))
?f<-(Head-Level-Mother-Daughters ?h ?lvl ?Mot $?pre)
(not (Mother_modified ?Mot))
(not (prawiniXi_id-Node ?Mot ?))
=>
        (assert (Mother_modified ?Mot))
        (assert (Head-Level-Mother-Daughters_new_fact ?h ?lvl ?Mot $?pre))
)


(defrule replace-daughters
(declare (salience 1500))
?used2<-(Head-Level-Mother-Daughters_new_fact ?head1 ?level ?mother1 $?pre ?mother $?post)
?used1<-(Head-Level-Mother-Daughters_new_fact ?head ?lvl ?mother $?daughters)
=>
        (retract ?used2)
        (assert (Head-Level-Mother-Daughters_new_fact ?head1 ?level ?mother1 $?pre $?daughters $?post))
)

(defrule replace_head_word_with_id
(declare (salience 1001))
?f<-(Head-Level-Mother-Daughters_new_fact ?head ?lvl ?Mot $?pre1 ?ph $?pos1)
?f1<-(Head-Level-Mother-Daughters ?head ?lvl ?Mot $?dau)
(parserid-word ?ph ?head)
=>
        (retract ?f ?f1)
        (assert (Head-Level-Mother-Daughters ?ph ?lvl ?Mot $?dau))
)

