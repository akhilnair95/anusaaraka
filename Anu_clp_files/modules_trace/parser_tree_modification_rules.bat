(watch all)
 (load "global_path.clp")
 (bind ?*path* (str-cat ?*path* "/Anu_clp_files/parser_tree_modification_rules.bclp"))
 (bload ?*path*)
 (load-facts "E_constituents_info_tmp1.dat")
 (load-facts "Node_category_tmp1.dat")
 (load-facts "sd_word_tmp.dat")
 (load-facts "sd_category_tmp1.dat") 
 (run)
 (save-facts "E_constituents_info_tmp2.dat" local Head-Level-Mother-Daughters)
 (save-facts "Node_category_tmp2.dat" local  Node-Category)
 (clear)
 (exit)
