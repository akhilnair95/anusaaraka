(watch all)
 ; take prefered morph according to wsd root
 (load "global_path.clp")
 (bind ?*path* (str-cat ?*path* "/Anu_clp_files/preferred_morph_consistency_check.bclp"))
 (bload ?*path*)
 (load-facts "root.dat")
 (load-facts "preferred_morph.dat")
 (load-facts "wsd_facts_output.dat")
 (load-facts "morph.dat")
 (open "revised_preferred_morph.dat" morph_cons_fp "w")
 (open "revised_root.dat" rev_rt_fp "w")
 (run)
 (clear)
 (exit)
