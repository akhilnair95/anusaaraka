(watch all)
 ;=================================  LANGUAGE GENERATION MODULE =================================
 ; Determine gender of all hindi words
 (load "global_path.clp")
 (bind ?*path* (str-cat ?*path* "/Anu_clp_files/gender_info.bclp"))
 (bload ?*path*)
 (load-facts "meaning_to_be_decided.dat")
 (load-facts "cat_consistency_check.dat")
 (load-facts "original_word.dat")
 (load-facts "hindi_meanings_tmp1.dat")
 (open "gender_tmp.dat" gen_fp "w")
 (run)
 (clear)
 (exit)
