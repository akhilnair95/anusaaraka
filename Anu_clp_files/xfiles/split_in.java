import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class split_in {
	
	// Figures out the i/p.dat and o/p.dat files of args[0] and puts it to args[1].ip.dat args[1].op.dat 
	
	public static void main(String[] args){

		String line;
		
		try {
			Scanner scn = new Scanner(new File(args[0]));
			while(scn.hasNextLine()){
				line = scn.nextLine().trim();
				// check for "(load-facts"
				if(line.startsWith("(load-facts")){
					System.out.println(line.substring(13, line.length() - 2));
				}
			}
			scn.close();
		} catch (FileNotFoundException e) {e.printStackTrace();}
		
	}


}
